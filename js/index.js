"use strict"

class Employee {
	constructor(name, age, salary) {
		this._name = name;
		this._age = age;
		this._salary = salary;
	}

	set name(newName) {
		this._name = newName;
	}

	get name() {
		return this._name;
	}

	set age(newAge) {
		this._age = newAge;
	}

	get age() {
		return this._age;
	}

	set salary(newSalary) {
		this._salary = newSalary;
	}

	get salary() {
		return this._salary;
	}
}

class Programmer extends Employee {
	constructor(lang, ...parent) {
		super(...parent);
		this.lang = lang;
	}

	get salary() {
		return super.salary * 3;
	}

	set salary(newSalary) {
		super.salary = newSalary;
	}


}

let admin = new Programmer("English", "Denis", 40, 150);
let anotherAdmin = new Programmer("Ukrainian", "Roman", 50, 200);
// console.log(admin.salary);
// console.log(anotherAdmin.salary);

console.log(admin, anotherAdmin)

